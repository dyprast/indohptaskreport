<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShoppingWallet extends Model
{
    public function shoppingWalletReports()
    {
        return $this->hasMany(ShoppingWalletReport::class);
    }
}
