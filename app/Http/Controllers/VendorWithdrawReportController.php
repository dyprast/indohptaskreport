<?php

namespace App\Http\Controllers;

use App\Model\VendorWithdrawReport;
use Illuminate\Http\Request;

class VendorWithdrawReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\VendorWithdrawReport  $vendorWithdrawReport
     * @return \Illuminate\Http\Response
     */
    public function show(VendorWithdrawReport $vendorWithdrawReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\VendorWithdrawReport  $vendorWithdrawReport
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorWithdrawReport $vendorWithdrawReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\VendorWithdrawReport  $vendorWithdrawReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorWithdrawReport $vendorWithdrawReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\VendorWithdrawReport  $vendorWithdrawReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorWithdrawReport $vendorWithdrawReport)
    {
        //
    }
}
