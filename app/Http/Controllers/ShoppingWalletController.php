<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\ShoppingWallet;
use App\Model\ShoppingWalletReport;
use App\User;

use DB;

class ShoppingWalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $d['users'] = User::orderBy("name", "ASC")->get();
        $d['shopping_wallets'] = ShoppingWallet::orderBy('id', "DESC")->get();
        return view('app.ShoppingWallet.index', $d);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $c = ShoppingWallet::orderBy("id", "DESC")->first();
        $d = new ShoppingWallet;
        if(!empty($c)){
            $shoppingWalletID = $c->id + 1;
        }
        else{
            $shoppingWalletID = 1;
        }
        $d->id = $shoppingWalletID;
        $d->task = $request->input('task');
        $d->nominal = $request->input('nominal');
        $d->date = $request->input('date');
        $d->payment = $request->input('payment');

        $d->save();

        $userID = $request->input('user_id');

        $whereArray = array('shopping_wallet_id' => $shoppingWalletID);
        $query = DB::Table('shopping_wallet_reports');
        foreach($whereArray as $field => $value) {
            $query->where($field, $value);
        }
        $query->delete();

        if(!empty($userID)){
            foreach($userID as $res){
                $e = new ShoppingWalletReport;
                $e->user_id = $res;
                $e->shopping_wallet_id = $shoppingWalletID;
    
                $e->save();
            }
        }

        return back()->with("alertStore", $request->input('task'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShoppingWallet  $shoppingWallet
     * @return \Illuminate\Http\Response
     */
    public function show(ShoppingWallet $shoppingWallet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShoppingWallet  $shoppingWallet
     * @return \Illuminate\Http\Response
     */
    public function edit(ShoppingWallet $shoppingWallet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShoppingWallet  $shoppingWallet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShoppingWallet $shoppingWallet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShoppingWallet  $shoppingWallet
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShoppingWallet $shoppingWallet)
    {
        $d = $shoppingWallet;
        $task = $d->task;
        $d->delete();

        return back()->with("alertDestroy", $task);
    }
}
