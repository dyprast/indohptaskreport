<div class="modal fade" id="shoppingWalletCreateModal" tabindex="-1" role="dialog" aria-labelledby="shoppingWalletCreateModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-secondary">
                <h5 class="modal-title text-white" id="shoppingWalletCreateLabel">Create New Shopping Wallet Report</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="shoppingWalletCreateForm" method="POST" action="{{ route('shoppingwallets.store') }}">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="task">Task</label>
                        <input type="text" name="task" class="form-control" placeholder="Enter Task" required>
                    </div>
                    <div class="form-group">
                        <label for="card_id">Assignee</label>
                        <select name="user_id[]" class="select2 select2-multiple" multiple="multiple" multiple data-placeholder="Choose ...">
                            @foreach($users as $res)
                            <option value="{{ $res->id }}">{{ $res->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nominal">Nominal</label>
                        <input type="number" name="nominal" id="nominal" class="form-control" placeholder="Nominal" required>
                    </div>
                    <div class="form-group">
                        <label>Date</label>
                        <input id="date-close" name="date" type="text" class="form-control" required>
                        <span class="font-13 text-muted">e.g "MM/DD/YYYY"</span>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="payment">Payment</label>
                            <select name="payment" class="form-control" placeholder="Payment" required>
                                <optgroup label="Bank Transfer Manual"></optgroup>
                                <option value="Bank BCA">Bank BCA</option>
                                <option value="Bank BNI">Bank BNI</option>
                                <option value="Bank MANDIRI">Bank MANDIRI</option>
                                <option value="Bank BRI">Bank BRI</option>
                                <option value="Bank BTN">Bank BTN</option>
                                <option value="Bank BTPN">Bank BTPN</option>
                                <option value="Bank CIMB">Bank CIMB</option>
                                <option value="Bank BJB">Bank BJB</option>
                                <option value="Bank DANAMON">Bank DANAMON</option>
                                <option value="Bank DBS">Bank DBS</option>
                                <option value="Bank Muamalat">Bank Muamalat</option>
                                <option value="Bank Sinarmas">Bank Sinarmas</option>
                                <option value="Bank Lainnya">Bank Lainnya</option>
                                </optgroup>
                                <optgroup label="Xendit"></optgroup>
                                <option value="Xendit Alfamart">Alfamart</option>
                                <option value="Xendit Virtual Account">Virtual Account</option>
                                <option value="Xendit OVO">OVO</option>
                                <optgroup label="Lainnya"></optgroup>
                                <option value="Pulsa">Pulsa</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary rounded">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    let selectMitra = document.getElementById('mitra')

    selectMitra.addEventListener('change', function() {
        let inputPrice = document.getElementById('price')
        inputPrice.value = selectMitra.options[this.selectedIndex].getAttribute('price')
    })
</script>